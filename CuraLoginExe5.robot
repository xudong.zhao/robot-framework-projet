*** Settings ***
Documentation    Un premier test d'accès à la page de login.
Library    SeleniumLibrary

*** Variables ***
${url}    https://katalon-demo-cura.herokuapp.com
${browser}    chrome

*** Keywords ***
Goto cura
    Open Browser    ${url}    ${browser}
    Maximize Browser Window
    Wait Until Element Is Visible    id:btn-make-appointment    timeout=10
    Click Element    id:btn-make-appointment

login
    [Arguments]    ${login}    ${pwd}
    Wait Until Page Contains Element    id:txt-username    timeout=10
    Input Text    id:txt-username    ${login}
    Input Text    id:txt-password    ${pwd}
    Click Element    id:btn-login

saisir formulaire
    Wait Until Element Is Visible    id:combo_facility    timeout=10
    Select From List By Label    id:combo_facility    Hongkong CURA Healthcare Center
    Click Element    id:chk_hospotal_readmission
    Click Element    id:radio_program_medicaid
    Input Text    id:txt_visit_date    02/07/2022
    Input Text    id:txt_comment    Ceci est un commentaire
    Click Element    id:btn-book-appointment
    #Wait Until Element Is Visible    id:appointment_confirmation    timeout=10
    Page Should Contain Element    xpath=//h2[contains(text(),'Appointment Confirmation')]


go Homepage
    Click Element    xpath=//a[contains(text(),'Go to Homepage')]
    
    

*** Test Cases ***
Passant
    Goto cura
    login    John Doe    ThisIsNotAPassword
    saisir formulaire
    go Homepage
    Close Browser


Non passant
    [Tags]    nonPassant
    Goto cura
    login    John Doe    123
    saisir formulaire
    go Homepage
    Close Browser

